package ar.com.nmv.test;

import ar.com.nmv.datos.ClienteDaoJDBC;
import ar.com.nmv.datos.Conexion;
import ar.com.nmv.dominio.Cliente;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Test {

    public static void main(String[] args) {
        Connection conexionTransaccional = null;

        try {
            conexionTransaccional = Conexion.getConnection();
            if (conexionTransaccional.getAutoCommit()) {
                conexionTransaccional.setAutoCommit(false);

                ClienteDaoJDBC clienteDao = new ClienteDaoJDBC(conexionTransaccional);

                Cliente cliente = new Cliente(4, "Carlos", "Juarez", "cjuarez@mail.com", "34343434", 900);

                clienteDao.actualizar(cliente);
                
//                Cliente cliente2 = new Cliente("Esteban", "Quito", "equito@mail.com", "44556677", 400);
//                
//                clienteDao.insertar(cliente2);
//                
//                Cliente cliente3 = new Cliente(2);
//
//                clienteDao.eliminar(cliente3);

                conexionTransaccional.commit();
                System.out.println("Se ha realizado un commit");

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            System.out.println("Realizando Rollback");
            try {
                conexionTransaccional.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace(System.out);
            }
        }

    }

}
