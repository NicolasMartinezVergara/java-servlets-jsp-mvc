Proyecto del curso “Universidad Java 2021, de cero a experto” (UDEMY)
Aplicación de control clientes utilizando JDBC para conexión a MySQL.
Aplicación de capa web con diseño MVC, utilizando clases de modelo, servlets y JSPs.

El objetivo de este ejercicio fue realizar una aplicación java para gestionar una lista de clientes de un negocio X, con su nombre, apellido,
saldo adeudado. Además también tener un control del número total de clientes y el saldo total de los deudores.
La idea principal fue trabajar más el concepto de patrón de diseño MVC para la capa web, más el manejo de base de datos con JDBC, recuperando 
los objetos desde la base de datos columna por columna (en lugar de objetos completos)
Fue el primer ejercicio con manejo de patrón MVC, con datos desde MySQL.

Desarrollado con Apache Netbeans, utilizando Java Empresarial (JavaEE version 8), y JDK 1.8.
Administrador de librerías Maven.
Base de datos utilizada MySQL 8 con MySQL Workbench.
Todas estas dependencias del proyecto descargadas mediante la configuración del archivo pom.xml.
Capa de datos utilizando JDBC.	

El servidor de aplicaciones es Glassfish, versión 5.0.
Servidor en localhost:8080 .

La clase de dominio o modelo (Cliente) está definida sin anotaciones especiales, dado que no se está implementando ningún Framework específico.
Sólo sus atributos, métodos get y set, el método toString, un constructor vacío (y más constructores si fuesen necesarios).

Se realiza la conexión por medio de JDBC y Mysql Driver en la ruta java-servlets-jsp-mvc/PracticaMVC/src/main/java/ar/com/nmv/datos, 
clase Conexion (previa configuración en el pom.xml para MySQL connector).
Aquí se crea el método DataSource para el manejo de pool de conexiones, y se crean los métodos para abrir y cerrar los objetos de
tipo Connection, Statement y ResultSet (esto cada vez que se realiza una operación con el servidor).
MySQL en localhost:3306 .

En la capa de datos también se encuentra la clase DAO de Cliente (en este caso, es la clase directamente, sin interface, la idea fue centrarse
en la práctica del manejo de datos en sí, por eso tampoco hay capa de servicio en este ejercicio).
En esta clase encuentran los métodos para interactuar con la base de datos.
Por cada método, se abre y se cierra una conexión (todo declarado en el método).
Cada método es mandar a llamar a una constante de la clase, que almacenan un String de tipo SQL para que se ejecute esa sentencia
(SELECT, INSERT, UPDATE, DELETE, con sus respectivos parámetros), por medio de los objetos Connection, PreparedStatement y 
procesando el resultado con ResultSet.

La capa Web está desarrollada siguiendo el patrón de diseño MVC (Model-View-Controller).
Esta contiene en este caso un Servlet, que recibe las peticiones del cliente por medio de las vistas (JSP´s), y a su vez se conecta con la capa 
de datos y con las clases de modelo (se crea una instancia de la clase DAO para poder acceder a sus métodos).
Dentro de la carpeta java-servlets-jsp-mvc/PracticaMVC/src/main/java/ar/com/nmv/web, se encuentran el Servlet, clase que extiende de la interface HttpServlet, 
para el manejo de las peticiones (request), las respuestas (response).
Tanto para agregar registros, modificarlos, eliminarlos, recuperar datos y redireccionar entre los diferentes JSP´s (en algunos casos, realizando
un "forward" con la petición y la respuesta).
Método doGet para links, método doPost para formularios.
Este servlet tiene un único path para poder acceder desde el JSP, con varios métodos para recibir parámetros del objeto "request", y en base
a esos parámetros que lleguen con la petición, poder ejecutar alguna acción.

En la ruta java-servlets-jsp-mvc/PracticaMVC/src/main/webapp se encuentra el archivo index.jsp, y dentro de src/main/webapp/WEB-INF se encuentran los JSP´s 
a desplegar para el cliente.
Los JSP´s permiten utilizar "expression languaje" con código Java, para un mejor manejo de los datos de los componentes Java Beans.
En el JSP de listadoClientes se utiliza además el concepto de "taglibs", con etiquetas especiales para trabajar sobre las vistas. En este caso,
se importan los tags de Core mediante el prefijo "c", lo que permite contar con algunas funciones como, por ejemplo, iterar una lista
dentro del mismo JSP.
También tags de JSTL con prefijo "fmt", que permite por ejemplo trabajar con el formato de los números sobre los mismos JSP´s (tipo moneda, por 
ejemplo).

Las pruebas se realizan por medio de la clase Test en la ruta java-servlets-jsp-mvc/PracticaMVC/src/main/java/ar/com/nmv/test, con un método 
Main para simular a un cliente y realizar pruebas para chequear que esté bien configurada la conexión a la base de datos.
Todas pruebas por la consola standard.


